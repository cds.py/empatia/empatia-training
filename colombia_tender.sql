select p.ocid, tender_amount, budget_amount, buyer_id,
       extract(month from (tender_tenderperiod_start_date)::timestamp) as tender_month,
left(replace(classification_id, '1.', ''), 4) as item_classification,
       procurement_method.code as procurement_method_desc,
       category.code as category_desc,
              procurement_method.value as procurement_method,
       category.value as category
from colombia.procurement p
join colombia.tender_items i on p.data_id = i.data_id and i.classification_id is not null
join colombia.parametros procurement_method on p.tender_procurementmethoddetails = procurement_method.key
join colombia.parametros category on p.tender_mainprocurementcategory = category.key
where framework_agreement is null and buyer_id is not null and buyer_id != '' and tender_amount > 0
and tender_amount is not null and budget_amount is not null and (tender_currency is null or tender_currency  = 'COP');