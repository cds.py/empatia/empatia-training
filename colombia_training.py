#!/usr/bin/python
# -*- coding: utf-8 -*-
from sklearn.ensemble import IsolationForest
from sklearn.tree import DecisionTreeClassifier
from joblib import dump

from database import get_engine
import pandas as pd

# para determinar si el proceso es muy anomalo o medio anomalo se usa el primer cuartil.
QUARTIL_1 = -0.13

modalidades = [

    "Selección Abreviada de Menor Cuantía (Ley 1150 de 2007)",
    "Selección Abreviada del literal h del numeral 2 del artículo 2 de la Ley 1150 de 2007",
    "Selección Abreviada de Menor Cuantía",
    "Contratación Directa (Ley 1150 de 2007)",
    "Enajenación de bienes con sobre cerrado",
    "Selección Abreviada servicios de Salud",
    "Contratación Directa (con ofertas)",
    "Asociación Público Privada",
    "Licitación pública",
    "Enajenación de bienes con subasta",
    "Licitación obra pública",
    "Contratación régimen especial (con ofertas)",
    "Contratación directa",
    "Contratos y convenios con más de dos partes",
    "Contratación Mínima Cuantía",
    "Mínima cuantía",
    "Contratación régimen especial",
    "Concurso de Méritos con Lista Corta",
    "Selección abreviada subasta inversa",
    "Régimen Especial",
    "Seleccion Abreviada Menor Cuantia Sin Manifestacion Interes",
    "Subasta",
    "Concurso de méritos abierto"

]

categorias = [
    "works",
    "services",
    "goods"
]

# columnas que se quitan para el arbol de decision, procurement_method, category seran reemplazadas
# por columnas dicotomicas
decision_tree_drop_columns = ['ocid', 'procurement_method', 'category', 'buyer_id']

# estas variables son las removidas para el random forest, por ser categoricas
variables_categoricas = ['procurement_method_desc', 'category_desc']

# estas son las columnas usadas por el modelo
llamados_columnas_en_orden = ['ocid',
                              'tender_amount',
                              'tender_month',
                              'procurement_method',
                              'category',
                              'item_classification',
                              'buyer_id',
                              'category_desc',
                              'procurement_method_desc']


def score_a_nominal(row):
    if row['score'] > 0:
        return 0
    elif QUARTIL_1 <= row['score'] <= 0:
        return 1
    else:
        return 2


def nominal_a_score(score):
    if score > 0:
        return 'green'
    elif QUARTIL_1 <= score <= 0:
        return 'yellow'
    else:
        return 'red'


def get_decision_tree(data):
    data = prepare_decision_tree_data(data)
    y_train = data['score_label'].to_numpy()
    x_train = data.drop(['score_label'], axis=1).to_numpy()
    estimator = DecisionTreeClassifier(random_state=0)
    estimator.fit(x_train, y_train)
    return estimator


def get_marca(marca_label, marca_row):
    return 1 if marca_label in str(marca_row) else 0


def prepare_decision_tree_data(data):
    columnas_categoricas = categorias + modalidades + ['score_label']
    llamado_columnas = llamados_columnas_en_orden + columnas_categoricas
    llamado_columnas = [item for item in llamado_columnas if item not in decision_tree_drop_columns]
    data['score_label'] = data.apply(lambda row: score_a_nominal(row), axis=1)
    data = data.drop(['score'], axis=1)

    # Con esto se convierte cada una de estos valores categóricos
    # en una columna para poder interpretarlo correctamente en el
    # árbol de decisión. Se crea una columna por marca, categoría, modalidad y forma de adjudicacion posible.
    # Si el valor corresponde a la columna entonces se completa con 1 si no con 0
    for categoria in categorias:
        data[categoria] = data.apply(lambda row: get_marca(categoria, row['category_desc']), axis=1)
    for modalidad in modalidades:
        data[modalidad] = data.apply(lambda row: get_marca(modalidad, row['procurement_method_desc']), axis=1)

    # Luego se borran las columnas numéricas que representan estos valores ca

    data = data[llamado_columnas]
    data = data.drop(decision_tree_drop_columns + variables_categoricas, axis=1, errors='ignore')
    return data


def get_decision_path(data, estimator):
    data = data.drop(['slug'], axis=1, errors='ignore')
    dt_data = prepare_decision_tree_data(data)
    x_train = dt_data.drop(['score_label'], axis=1).to_numpy()
    column_names = list(dt_data.columns)
    feature = estimator.tree_.feature
    node_indicator = estimator.decision_path(x_train)
    leave_id = estimator.apply(x_train)
    sample_id = 0
    node_index = node_indicator.indices[node_indicator.indptr[sample_id]:node_indicator.indptr[sample_id + 1]]
    regla = ""
    check = []
    for node_id in node_index:
        if leave_id[sample_id] == node_id:
            continue
        if column_names[feature[node_id]] not in check:
            valor = f'{int(x_train[sample_id, feature[node_id]]):,}'.replace(',', '.')
            if column_names[feature[node_id]] in modalidades:
                regla = regla + f'El proceso es {data.iloc[0]["procurement_method_desc"]}'
                check.extend(modalidades)
            if column_names[feature[node_id]] in categorias:
                categoria = data.iloc[0]["category_desc"]
                if categoria == 'goods':
                    categoria = 'Bienes'
                if categoria == 'services':
                    categoria = 'Servicios'
                if categoria == 'works':
                    categoria = 'Obras'
                regla = regla + f', la categoria es {categoria}'
                check.extend(categorias)
            if column_names[feature[node_id]] == 'tender_amount':
                regla = regla + f', el monto referencial es {valor} COL'
            if column_names[feature[node_id]] == 'tender_enquiry_period':
                regla = regla + f', el periodo de consultas es de {int(x_train[sample_id, feature[node_id]])} dias'
            if column_names[feature[node_id]] == 'tender_period':
                regla = regla + f', el periodo de licitación es de {int(x_train[sample_id, feature[node_id]])} dias'
        check.append(column_names[feature[node_id]])
    return regla


def prediction_to_ret(data, a_ret):
    modalidades = data['procurement_method_desc']
    categorias = data['category_desc']

    # Las quitamos para el IsolationForest
    data = data.drop(variables_categoricas + ['ocid', 'score', 'score_label'], axis=1)
    scores = clf.decision_function(data)[0]

    data['procurement_method_desc'] = modalidades
    data['category_desc'] = categorias
    data['score'] = scores
    a_ret['score'] = scores

    a_ret['rules'] = get_decision_path(data, dt)
    a_ret['risk'] = nominal_a_score(scores)
    return a_ret


engine = get_engine()
max_feature = 14
forest_size = 100
sample_size = 20

with open('colombia_tender.sql', 'r') as query:
    results = pd.read_sql_query(query.read(), engine)

# quitamos las columnas de descripcion para entrenar
to_train = results.drop(columns=['procurement_method_desc', 'category_desc', 'ocid'], axis=1)

# definimos el algoritmo
clf = IsolationForest(n_estimators=forest_size, max_samples=sample_size, behaviour='new',
                      random_state=0, n_jobs=1, max_features=len(to_train.columns), contamination='auto')

# entrenamos
clf.fit(to_train, y=None, sample_weight=None)
# guardamos el modelo entrenado
dump(clf, 'colombia.joblib')

# predecimos sobre lo entrenado
prediction = clf.predict(to_train)

# aqui vemos los scores obtenidos
scores = clf.decision_function(to_train)
predictions = pd.DataFrame()
predictions["predicted_class"] = prediction
predictions["score"] = scores
results['score'] = scores
predictions["slug"] = results["ocid"]
predictions["modalidad"] = results["procurement_method_desc"]

# guardamos las predicciones en un csv
predictions.to_csv('colombia.csv',
                   header=['predicted_class', 'Scores', 'slug', 'modalidad'], index=None, sep=',', mode='w')

# obtenemos el arbol de decision para interpretar los resultados
dt = get_decision_tree(results)
dump(dt, 'colombia_decision_tree.joblib')
# calculamos el resultado para cada fila
for index, row in results.iterrows():
    a_ret = {'ocid': row['ocid']}
    row = row.to_frame().T
    pred = prediction_to_ret(row, a_ret)
    if pred['risk'] == 'red':
        print(pred)
