# Empatia Training
Módulo de entrenamiento del proyecto EmpatIA, el cual es utilizado para generar los modelos de detección de anomalías
a ser utilizados por el módulo de análisis, y que consume los datos generados por el módulo de ETL

Los archivos principales son paraguay_training y colombia_training y están pensados para ser ejecutados localmente y en
ambiente de pruebas.

El script asume que existe una base de datos creada, con las tablas creadas por el modulo de ETL, el cual también se
encarga de luego ejecutar los modelos de predicción generados por este módulo.

El script para la creación de la tabla parámetros con la que los modelos fueron entrenados se encuentra en este repositorio.

Para reentrenar alguno de los dos países, basta con tener la base de datos y configurar su acceso en el archivo database.py, instalar las dependencias listadas en requirements.tx
y correr el archivo paraguay_training o colombia_training. Los mismos, si se ejecutan correctamente deben generar 3 archivos:
- pais.csv: con los scores para cada uno de los procesos entrenados. Un score negativo indica que el proceso es anormal, y uno positivo que es normal.
- pais.joblib: con el modelo de clasificación generado, a llevar al módulo de ETL para su utilización
- pais_decision_tree.joblib: con el modelo de interpretación, para tratar de interpretar el modelo de clasificación obtenido.

Los queries utilizados para cada país se encuentran en pais_tender.sql
En cada archivo de entrenamiento se encuentran también definidos algunas constantes utilizadas en el entrenamiento, como ser
las variables categoricas (modalidades, categorias, etc) y también los cuartiles. Los cuartiles son utilizados para clasificar
qué tan anormal es el proceso, pudiendo ir de red (mayor al primer cuartil), amarillo (entre el primer cuartil y los numeros
negativos) o verde (todos los scores positivos)

## Variables utilizadas en Paraguay:

- tender_amount: monto referencial del llamado
- tender_period: periodo entre la publicación del llamado y el cierre de ofertas en dias
- tender_enquiry_period: periodo de consultas en días
- tender_period_enquiry: periodo entre el cierre del llamado y el cierre de consultas en días
- award_criteria: forma de adjudicación (lote, items, combinado, total)
- procurement_method: método de contratación (licitación pública, etc)
- category: categoría
- buyer_id: codigo sicp de la entidad compradora

Los datos de entrenamiento todos los procesos de contratación (menos convenio marco) con fecha de publicación del llamado desde el año 2016
hasta 2020-12.


## Variables utilizadas en Colombia:

- tender_amount: monto referencial del llamado
- tender_month: mes del llamado
- procurement_method: método de contratación (licitación pública, etc)
- category: categoría
- buyer_id: codigo de la entidad compradora
- item_classification: clasificación de nivel 3 del item a comprar (solo se publica uno en tender)

Los datos de entrenamiento todos los procesos de contratación (menos los de TVEC) con fecha de publicación del release desde
el año 2019 hasta 2020-12.