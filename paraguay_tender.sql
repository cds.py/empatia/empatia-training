select distinct
                ocid,
       coalesce ((case when tender_currency = 'USD' then (
            (select compra from paraguay.cotizacion where fecha <=tender_date_published
           order by fecha desc limit 1) * tender_amount
           )
        else tender_amount end),0) as tender_amount,
         coalesce (case when budget_currency = 'USD' then (
            (select compra from paraguay.cotizacion where fecha <=planning_estimated_date
           order by fecha desc limit 1) * budget_amount
           )
        else budget_amount end, 0) as budget_amount,
       awardcreiteria.value as award_criteria,
                awardcreiteria.key as award_criteria_desc,

      coalesce(extract(day from tender_enquiryperiod_end_date - tender_enquiryperiod_start_date),0) as tender_enquiry_period,
      coalesce(extract(day from tender_tenderperiod_end_date - tender_tenderperiod_start_date),0) as tender_period,
      coalesce(extract(day from tender_tenderperiod_end_date - tender_enquiryperiod_end_date),0) as tender_period_enquiry,
       method.value as procurement_method,
       method.code as procurement_method_desc,
       replace(buyer_id, 'DNCP-SICP-CODE-', '') as buyer_id,
       category.value as category,
    category.code as category_desc,
       coalesce(tender_numberoftenderers::int, 0) as tender_numberoftenderers,
       tender_numberofenquiries
from paraguay.procurement
join paraguay.parametros awardcreiteria on awardcreiteria.key = tender_awardcriteria_details
join paraguay.parametros category on category.key = tender_mainprocurementcategorydetails
join paraguay.parametros method on method.key = tender_procurementmethoddetails
where (framework_agreement is false or framework_agreement is null) and tender_status is not null
and extract(year from tender_date_published) >= 2016