create table colombia.parametros
(
    key   text,
    type  text,
    value bigint,
    code  text
);


INSERT INTO colombia.parametros (key, type, value, code) VALUES ('Concurso de Méritos Abierto', 'procurement_method', 6, 'Concurso de méritos abierto');
INSERT INTO colombia.parametros (key, type, value, code) VALUES ('Licitación pública Obra Publica', 'procurement_method', 3, 'Licitación obra pública');
INSERT INTO colombia.parametros (key, type, value, code) VALUES ('Licitación Pública', 'procurement_method', 8, 'Licitación pública');
INSERT INTO colombia.parametros (key, type, value, code) VALUES ('Contratación Directa (Ley 1150 de 2007)', 'procurement_method', 1, 'Contratación Directa (Ley 1150 de 2007)');
INSERT INTO colombia.parametros (key, type, value, code) VALUES ('Selección abreviada subasta inversa', 'procurement_method', 2, 'Selección abreviada subasta inversa');
INSERT INTO colombia.parametros (key, type, value, code) VALUES ('Selección Abreviada servicios de Salud', 'procurement_method', 4, 'Selección Abreviada servicios de Salud');
INSERT INTO colombia.parametros (key, type, value, code) VALUES ('services', 'procurement_category', 5, 'services');
INSERT INTO colombia.parametros (key, type, value, code) VALUES ('Selección Abreviada de Menor Cuantía', 'procurement_method', 9, 'Selección Abreviada de Menor Cuantía');
INSERT INTO colombia.parametros (key, type, value, code) VALUES ('goods', 'procurement_category', 10, 'goods');
INSERT INTO colombia.parametros (key, type, value, code) VALUES ('Selección Abreviada de Menor Cuantía (Ley 1150 de 2007)', 'procurement_method', 11, 'Selección Abreviada de Menor Cuantía (Ley 1150 de 2007)');
INSERT INTO colombia.parametros (key, type, value, code) VALUES ('Contratación régimen especial (con ofertas)', 'procurement_method', 12, 'Contratación régimen especial (con ofertas)');
INSERT INTO colombia.parametros (key, type, value, code) VALUES ('Concurso de Méritos con Lista Corta', 'procurement_method', 13, 'Concurso de Méritos con Lista Corta');
INSERT INTO colombia.parametros (key, type, value, code) VALUES ('Mínima cuantía', 'procurement_method', 15, 'Mínima cuantía');
INSERT INTO colombia.parametros (key, type, value, code) VALUES ('Régimen Especial', 'procurement_method', 16, 'Régimen Especial');
INSERT INTO colombia.parametros (key, type, value, code) VALUES ('Enajenación de bienes con subasta', 'procurement_method', 17, 'Enajenación de bienes con subasta');
INSERT INTO colombia.parametros (key, type, value, code) VALUES ('Contratación Mínima Cuantía', 'procurement_method', 18, 'Contratación Mínima Cuantía');
INSERT INTO colombia.parametros (key, type, value, code) VALUES ('Selección Abreviada del literal h del numeral 2 del artículo 2 de la Ley 1150 de 2007', 'procurement_method', 19, 'Selección Abreviada del literal h del numeral 2 del artículo 2 de la Ley 1150 de 2007');
INSERT INTO colombia.parametros (key, type, value, code) VALUES (null, 'procurement_category', 23, null);
INSERT INTO colombia.parametros (key, type, value, code) VALUES ('Contratos y convenios con más de dos partes', 'procurement_method', 20, 'Contratos y convenios con más de dos partes');
INSERT INTO colombia.parametros (key, type, value, code) VALUES ('Enajenación de bienes con sobre cerrado', 'procurement_method', 21, 'Enajenación de bienes con sobre cerrado');
INSERT INTO colombia.parametros (key, type, value, code) VALUES ('Contratación directa', 'procurement_method', 22, 'Contratación directa');
INSERT INTO colombia.parametros (key, type, value, code) VALUES ('Subasta', 'procurement_method', 25, 'Subasta');
INSERT INTO colombia.parametros (key, type, value, code) VALUES ('Seleccion Abreviada Menor Cuantia Sin Manifestacion Interes', 'procurement_method', 26, 'Seleccion Abreviada Menor Cuantia Sin Manifestacion Interes');
INSERT INTO colombia.parametros (key, type, value, code) VALUES ('Asociación Público Privada', 'procurement_method', 27, 'Asociación Público Privada');
INSERT INTO colombia.parametros (key, type, value, code) VALUES ('works', 'procurement_category', 28, 'works');
INSERT INTO colombia.parametros (key, type, value, code) VALUES ('Contratación régimen especial', 'procurement_method', 29, 'Contratación régimen especial');
INSERT INTO colombia.parametros (key, type, value, code) VALUES ('Contratación Directa (con ofertas)', 'procurement_method', 30, 'Contratación Directa (con ofertas)');
INSERT INTO colombia.parametros (key, type, value, code) VALUES ('Concurso de méritos abierto', 'procurement_method', 6, 'Concurso de méritos abierto');
INSERT INTO colombia.parametros (key, type, value, code) VALUES ('Licitación obra pública', 'procurement_method', 3, 'Licitación obra pública');
INSERT INTO colombia.parametros (key, type, value, code) VALUES ('Licitación pública', 'procurement_method', 8, 'Licitación pública');